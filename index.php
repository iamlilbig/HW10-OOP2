<?php
class User{
    protected $userName;
    public function getUserName() { 
        return $this->userName;
    }
    public function setUserName($userName){
        $this->userName = $userName;
    }
}
interface Author{
    function setAuthorPrivileges($array);
    function getAuthorPrivileges();
}
interface Editor{
    function setEditorPrivileges($array);
    function getEditorPrivileges();
}
class AuthorEditor extends User implements Editor,Author {
    public $editorPrivilegesArray;
    public $authorPrivilegesArray;
    function setEditorPrivileges($array){
        $this->editorPrivilegesArray = $array;
    }
    function getEditorPrivileges(){
        return $this->editorPrivilegesArray;
    }
    function setAuthorPrivileges($array){
        $this->authorPrivilegesArray = $array;
    }
    function getAuthorPrivileges(){
        return $this->authorPrivilegesArray;
    }
}
$user1 = new AuthorEditor();
$user1->setUserName("Balthazar");
$user1->setAuthorPrivileges(["write text"=>10,"add punctuation"=>8]);
$user1->setEditorPrivileges(["edit text"=>6,"edit punctuation"=>4]);

$userName = $user1->getUserName();
$userPrivileges = array_merge($user1->getAuthorPrivileges(),$user1->getEditorPrivileges());
echo $userName . " has the following privileges: ";
foreach($userPrivileges as $privilege)
{
echo " {$privilege},";
}
echo ".";